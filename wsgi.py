from flask import Flask, request, jsonify
application = Flask(__name__)

@application.route("/", methods=['POST'])
def hello():
    input_json = request.get_json(force=True)
    # print ('data from client:' + input_json.test)
    return jsonify(input_json)

if __name__ == "__main__":
    application.run()
